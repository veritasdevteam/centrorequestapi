﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentroRequestAPI
{
    class Program
    {
        static long ClaimID = 0;
        static long InspectNo = 0;
        const string sUserName = "Veritasgp";
        const string sPassword = "V3r1t@$2021";
        const string sCustomerCode = "V06170";
        static string sRequesterName;
        static string sRequesterPhone;
        static string sRequesterEMail;
        static string sContractHolder;
        static string sVehYear;
        static string sVehMake;
        static string sVehModel;
        static string sVIN;
        static string sMileage;
        static string sContractNo;
        static string sClaimNo;
        static string sServiceName;
        static string sAddr1;
        static string sAddr2;
        static string sCity;
        static string sState;
        static string sZip;
        static string[] sInspectReason = new string[1];
        static string sPhone;
        static string sContact;
        const string sCustomerReference = "";
        static string SQL;
        const string sCon = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
        static DBO.clsDBO clCL = new DBO.clsDBO();
        static DBO.clsDBO clC = new DBO.clsDBO();
        static DBO.clsDBO clCI = new DBO.clsDBO();

        static void Main(string[] args)
        {


            for (int cnt = 0; cnt < args.Length; cnt++)
            {
                if (args[cnt] == "-claimid")
                {
                    ClaimID = Convert.ToInt32(args[cnt + 1]);
                }
                if (args[cnt] == "-inspectno")
                {
                    InspectNo = Convert.ToInt32(args[cnt + 1]);
                }
            }

            GetClaimInfo();
        }

        static void GetClaimInfo()
        {
            SQL = "select * from claim " +
                "where claimid = " + ClaimID.ToString();
            clCL.OpenDB(SQL, sCon);
            if (clCL.RowCount > 0)
            {
                clCL.GetRow();
                sClaimNo = clCL.get_Fields("claimno");
                sMileage = clCL.get_Fields("lossmile");
                sContact = clCL.get_Fields("sccontactinfo");
                GetContractInfo();
            }
        }

        static void GetContractInfo()
        {
            SQL = "select * from contract " +
                "where contractid = " + clCL.get_Fields("contractid");
            clC.OpenDB(SQL, sCon);
            if (clC.RowCount > 0)
            {
                clC.GetRow();
                sContractHolder = clC.get_Fields("fname") + " " + clC.get_Fields("lname");
                sContractNo = clC.get_Fields("contractno");
                sVehYear = clC.get_Fields("year");
                sVehMake = clC.get_Fields("make");
                sVehModel = clC.get_Fields("model");
                GetServiceCenterInfo();
            }
        }

        static void GetServiceCenterInfo()
        {
            DBO.clsDBO clSC = new DBO.clsDBO();
            SQL = "select * from servicecenter " +
                "where servicecenterid = " + clCL.get_Fields("servicecenterid");
            clSC.OpenDB(SQL, sCon);
            if (clSC.RowCount > 0)
            {
                clSC.GetRow();
                sServiceName = clSC.get_Fields("servicecentername");
                sAddr1 = clSC.get_Fields("addr1");
                sAddr2 = clSC.get_Fields("addr2");
                sCity = clSC.get_Fields("city");
                sState = clSC.get_Fields("state");
                sZip = clSC.get_Fields("zip");
                sPhone = clSC.get_Fields("phone");
                GetInspectInfo();
            }
        }
        static void GetInspectInfo()
        {

            SQL = "select * from claiminspection " +
                "where claimid = " + ClaimID.ToString() + " " +
                "and inspectno = " + InspectNo.ToString();
            clCI.OpenDB(SQL, sCon);
            if (clCI.RowCount > 0)
            {
                clCI.GetRow();
                sInspectReason[0] = clCI.get_Fields("InspectNote");
                GetUserInfo();
            }
        }
        static void GetUserInfo()
        {
            DBO.clsDBO clUI = new DBO.clsDBO();
            SQL = "select * from userinfo " +
                "where userid = " + clCL.get_Fields("assignedto");
            clUI.OpenDB(SQL, sCon);
            if (clUI.RowCount > 0)
            {
                clUI.GetRow();
                sRequesterName = clUI.get_Fields("fname") + " " + clUI.get_Fields("lname");
                sRequesterPhone = "8885724310";
                sRequesterEMail = clUI.get_Fields("email");
                SendRequest();
            }
        }

        static void SendRequest()
        {
            Centro.AuthenticateHeader AuthUser = new Centro.AuthenticateHeader
            {
                UserName = sUserName,
                Password = sPassword,
                CustomerCode = sCustomerCode
            };
            var Service = new Centro.CentroInspectionAPISoapClient();

            var sResp = Service.RequestInspection(AuthUser, sRequesterName, sRequesterPhone, sRequesterEMail,sContractHolder,sVehYear,
                sVehMake, sVehModel,sMileage,sVIN,sContractNo,ClaimID.ToString(),sInspectReason, sServiceName,sAddr1 + " " + sAddr2,sCity,sState
                ,sZip,sPhone,sContact,sCustomerReference,"");
        }
    }
}
